/*
 *  EP1 - MAC0422 - Abril de 2022
 *
 *  Integrantes:
 * 	 Arthur Pilone M. da Silva - n° USP: 11795450
 *	 Lorenzo Bertin Salvador   - n° USP: 11795356
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

/*
 *  Função que recebe um arquivo indicado por caminho e coloca proteção 000 (---------) nele.
 */
void protegePraCaramba(char caminho[]){
	if (chmod(caminho,0|0|0) == 0)	
		printf("Permissao do arquivo %s alterada para 000\n",caminho);
	else
		printf("Nao foi possivel alterar a permissao do arquivo %s\n",caminho);
}

/*
 *	Função que recebe um arquivo indicado por caminho e coloca proteção 777 (rwxrwxrwx) nele.
 */
void liberaGeral(char caminho[]){
	if (chmod(caminho,0777) == 0)
		printf("Permissao do arquivo %s alterada para 777\n",caminho);
	else
		printf("Nao foi possivel alterar a permissao do arquivo %s\n",caminho);
}

/*
 *	Função que executa o programa indicado por caminho e imprime seu código de retorno.
 */
void rodeVeja(char caminho[],char *envp[]){
	char * arg[] = {caminho,(char*)0};
	int status;		
	int return_code = -1;
	pid_t last_closed_child;
	pid_t pid = fork();
	if (pid == 0){
		execve(caminho,arg,envp);
	}
	else if (pid < 0){
		printf("fork() falhou\n");
	}
	else{
		do{
			last_closed_child = wait(&status);
		}while(last_closed_child != pid);
		
		if (WIFEXITED(status)){
			return_code = WEXITSTATUS(status);
			printf("=> programa '%s' retornou com codigo %d\n",caminho,return_code);
		}
		else{
			printf("Ocorreu um problema com o processo filho\n");
		}					
	}
}

/*
 *	Função que executa em background o programa indicado por caminho e imprime na tela
 * tanto a saída do programa quanto a saída do shell.
*/
void rode(char caminho[], char *envp[]){
	char * arg[] = {caminho,(char*)0};

	pid_t pid = fork();

	if(pid == 0){
		close(0);
		execve(caminho,arg,envp);
	}else if(pid < 0){
		printf("fork() falhou\n");
	}
}


int main(int argc, char *argv[],char *envp[]){

	while(1){

		char executavel[20];
		char caminho[100];

		printf("mac422shell: ");

		scanf("%s",executavel);
		if (strcmp(executavel,"exit") == 0)
			break;

		scanf("%s",caminho);

		if (access(caminho,F_OK) != 0){
			printf("Arquivo nao encontrado\n");
			continue;
		}

		if (strcmp(executavel,"protegepracaramba") == 0){
			protegePraCaramba(caminho);
		}
		else if (strcmp(executavel,"liberageral") == 0){
			liberaGeral(caminho);
		}
		else if (strcmp(executavel,"rodeveja") == 0){
			rodeVeja(caminho,envp);
		}
		else if (strcmp(executavel,"rode") == 0){
			rode(caminho,envp);
		}	
		else{
			printf("Comando nao encontrado\n");
			continue;
		}
		
	}

	return 0;

}


